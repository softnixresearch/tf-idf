ขั้นตอนการทดสอบดังนี้ครับ
1. Download sanook1.tar.gz  และ tfidf1.ipynb
2. สร้าง folder   ./jupyter/notebook/ , ./jupyter/data  
3. extract sanook1.tar.gz   ภายใต้ folder ./jupyter/data/
4. วาง file tfidf1.ipynb ไว้ที่ ./jupyter/notebook/
5. ติดตั้ง jupyter notebook library ที่ python ต้องการ
6. cd ./jupyter และ run command  > jupyter notebook ที่ folder นี้

สามารถทดสอบได้ตามปรกติ
